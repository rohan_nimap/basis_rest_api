## CRUD using Django Restful API

Performs CRUD operations using Django Restful Api

Django Version :- 2.2.4


### (Base) EndPoint :-
`127.0.0.1:8000/api/v1/products/`


### GET REQUEST
http://127.0.0.1:8000/api/v1/products/

output :-

```
[
    {
        "name": "oneplusone",
        "description": "sde S cS das dA ds D",
        "price": 35000,
        "category": {
            "id": 3,
            "name": "mobile"
        }
    },
    {
        "name": "rolex",
        "description": "watchawd",
        "price": 2400,
        "category": {
            "id": 2,
            "name": "watches"
        }
    },
    {
        "name": "nike",
        "description": "nike shoes",
        "price": 2000,
        "category": {
            "id": 1,
            "name": "shoes"
        }
    },
    {
        "name": "MI",
        "description": "phone",
        "price": 1000,
        "category": {
            "id": 3,
            "name": "mobile"
        }
    },
    {
        "name": "Audidas",
        "description": "Shoes",
        "price": 100,
        "category": {
            "id": 1,
            "name": "shoes"
        }
    },
    {
        "name": "nike",
        "description": "gareebo ke liye nhi hai",
        "price": 10,
        "category": {
            "id": 1,
            "name": "shoes"
        }
    },
    {
        "name": "nokia",
        "description": "phone like an hammer",
        "price": 2500,
        "category": {
            "id": 2,
            "name": "watches"
        }
    },
    {
        "name": "samsung",
        "description": "good phones",
        "price": 2500,
        "category": {
            "id": 3,
            "name": "mobile"
        }
    }
]
```

## Endpoint to Search Product based on its primary key

# GET Request

Endpoint :-

`http://127.0.0.1:8000/api/v1/products/3/`

Output :-
```
{
    "name": "nike",
    "description": "nike shoes",
    "price": 2000,
    "category": {
        "id": 1,
        "name": "shoes"
    }
}

```

## Search According to Category

`http://127.0.0.1:8000/api/v1/products/?category=1`

### Output :-
```
[
    {
        "name": "nike",
        "description": "nike shoes",
        "price": 2000,
        "category": {
            "id": 1,
            "name": "shoes"
        }
    },
    {
        "name": "Audidas",
        "description": "Shoes",
        "price": 100,
        "category": {
            "id": 1,
            "name": "shoes"
        }
    },
    {
        "name": "nike",
        "description": "gareebo ke liye nhi hai",
        "price": 10,
        "category": {
            "id": 1,
            "name": "shoes"
        }
    }
]
```
## Perform Update,Delete
Send PUT, DELETE Request to
`http://127.0.0.1:8000/api/v1/products/3/`



