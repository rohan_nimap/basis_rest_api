from rest_framework import serializers
from .models import ProductDetails

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        # Fields used in the employee model
        model = ProductDetails
        fields = '__all__'