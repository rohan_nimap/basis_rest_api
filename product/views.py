from django.shortcuts import render
from django.http import HttpResponse

from django.shortcuts import get_object_or_404 # if object is present show it or display 404
from rest_framework.views import APIView # NOrmal view cann return API Data
from rest_framework.response import Response # GIves Responses
from rest_framework import status # returns status
from .models import ProductDetails
from .serializers import ProductSerializer

# Create your views here.

class ProductList(APIView):
    # return all products 
    def get(self,request):
        # get all objects and saves them in product_all var
        product_all = ProductDetails.objects.all()
        # Serialize the data, many=True because there can be many records
        serializer = ProductSerializer(product_all, many=True)
        # Return the data serialized by serializer
        return Response(serializer.data)

    def post(self):
        pass
 




