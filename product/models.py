from django.db import models

# Create your models here.
class ProductDetails(models.Model):
    product_name = models.CharField(max_length=50)
    product_desc = models.CharField(max_length=50)
    product_category = models.CharField(max_length=25)

    def __str__(self):
        return self.product_name