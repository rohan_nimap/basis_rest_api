from rest_framework import serializers
from . import models

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = models.Category


class ProductSerializer(serializers.ModelSerializer):

    #category = serializers.StringRelatedField()
    category = CategorySerializer()

    class Meta:
        fields = ['name','description','price','category']
        model = models.Products

class ProductCreateSerializer(serializers.ModelSerializer):

    #category = serializers.StringRelatedField()
    # category = CategorySerializer()

    class Meta:
        fields = ['name','description','price','category']
        model = models.Products