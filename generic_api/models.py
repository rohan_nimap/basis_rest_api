# Create your models here.
# posts/models.py
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta :
        # Explicitly define the name of the table
        db_table = "categories"

class Products(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.IntegerField()
    category = models.ForeignKey(Category,on_delete=models.CASCADE,default=1)

    def __str__(self):
        return self.name

    class Meta : 
        # Explicitly define the name of the products
        db_table = 'products'