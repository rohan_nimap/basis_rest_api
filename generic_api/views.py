from django.shortcuts import render
from django.http import request
# Create your views here.
from rest_framework import generics

from .models import Products
from .serializers import ProductSerializer,ProductCreateSerializer


class ProductList(generics.ListAPIView,generics.ListCreateAPIView):
    serializer_class = ProductSerializer
    '''
    queryset = Products.objects.all()
    pcategory = request.query_params.get('category', None)
    '''
    
    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Products.objects.all()
        pchar = self.request.query_params.get('category', None)
        if pchar is not None:
            queryset = queryset.filter(category_id=pchar)
        return queryset


class ProductCreateAPIView(generics.CreateAPIView):
    serializer_class = ProductCreateSerializer
    queryset = Products.objects.all()


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    
    queryset = Products.objects.all()
    serializer_class = ProductCreateSerializer
